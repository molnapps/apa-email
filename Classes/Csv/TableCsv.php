<?php

class TableCsv
{
	public function getHeaders()
	{
		return ["Fattura n", "del", "scadenza", "valuta", "importo"];
	}

	public function getRows()
	{
		// This is just a stub.
		// This data will most likely come from a database
		return [
			["297_20","07.01.2020","31.03.2020","EUR","537,14"],
			["1591_20","15.01.2020","31.03.2020","EUR","504,78"],
		];
	}
}