<?php

class TxtToHtmlConverter
{
	private $filePath;

	public function __construct($filePath)
	{
		$this->filePath = $filePath;
	}
	
	public function get()
	{
		$result = $this->getTxtFileContents();
		$result = $this->encodeHtmlEntities($result);
		$result = $this->convertNewLinesToBrTags($result);
		$result = $this->restoreStrongTags($result);
		return $result;
	}

	private function getTxtFileContents()
	{
		return file_get_contents($this->filePath);
	}

	private function encodeHtmlEntities($content)
	{
		// e.g. à becomes &agrave;
		return htmlentities($content);
	}

	private function convertNewLinesToBrTags($content)
	{
		return nl2br($content);
	}

	private function restoreStrongTags($content)
	{
		// <strong> tag was converted to &lt;strong&gt; by htmlentities()
		// We must restore it
		return str_replace(
			['&lt;strong&gt;', '&lt;/strong&gt;'], 
			['<strong>', '</strong>'], 
			$content
		);
	}
}