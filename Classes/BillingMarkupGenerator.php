<?php

class BillingMarkupGenerator extends AbstractMarkupGenerator
{
	protected $bodyPartial = '_billing.html';
	
	protected function replaceBodyTokens($markupTemplate)
	{
		$markupTemplate = $this->replaceDictionaryTokens($markupTemplate);
		$markupTemplate = $this->replaceTableTokens($markupTemplate);
		return $markupTemplate;
	}

	private function replaceDictionaryTokens($markupTemplate)
	{
		return $this->replaceTokens(
			(new DictionaryBillingCsv)->getRows(), 
			$markupTemplate
		);
	}

	private function replaceTableTokens($markupTemplate)
	{
		return $this->replaceTokens(
			(new BillingTableMarkupProvider($this->htmlTemplateBasePath))->getRows(),
			$markupTemplate
		);
	}
}