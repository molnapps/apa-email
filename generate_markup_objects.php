<?php

require_once "Classes/Csv/DictionaryBillingCsv.php";
require_once "Classes/Csv/RecipientCsv.php";
require_once "Classes/Csv/HeadingCsv.php";
require_once "Classes/Csv/TableCsv.php";
require_once "Classes/TxtToHtmlConverter.php";
require_once "Classes/BillingTableMarkupProvider.php";

require_once "Classes/AbstractMarkupGenerator.php";
require_once "Classes/BillingMarkupGenerator.php";
require_once "Classes/ThankYouMarkupGenerator.php";

(new BillingMarkupGenerator('./html_template'))->output('./output.billing.html');
(new ThankYouMarkupGenerator('./html_template'))->output('./output.thanks.html');